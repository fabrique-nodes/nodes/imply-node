from fabrique_nodes_core.validators import validate_node
from fabrique_nodes_core.convertors import convert_node_data_to_config
from fabrique_nodes_core import NodeData, root_port, is_valid_port
from fabrique_nodes_core.db.fake_db import FakeDB
from unittest.mock import patch

import os
import sys

from dotenv import load_dotenv
load_dotenv()

cur_file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f"{cur_file_dir}/..")


from src.node import Node as ImplyWrite


def test_positive_validations():
    validate_node(ImplyWrite)


@patch('fabrique_nodes_core.crypto.DB', new=FakeDB)
def test_general():
    API_KEY = os.environ['API_KEY']
    API_ENDPOINT = os.environ['API_ENDPOINT']
    user_id = '_user_id_'

    ui_config = {
        "endpoint": API_ENDPOINT,
        "api_secret_key": API_KEY,
        "access_key_hash": ''
    }

    node_data = NodeData(
        name="",
        g_ports_in=[[root_port], [is_valid_port]],
        ui_config=ui_config.copy()
    )

    # cfg_read = convert_node_data_to_config(node_data_read)
    # cfg_write = convert_node_data_to_config(node_data_write)

    upd_node_data = ImplyWrite.make_hashed_outputs_cb(node_data, user_id)

    imply_write = ImplyWrite(convert_node_data_to_config(upd_node_data), user_id)
    test_values = [[{'key': 'value0'}, {'key': 'value1'}, {'key': 'value2'}]]
    imply_write.process_many(test_values)
