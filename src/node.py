import os
import sys
import json
import requests

from fabrique_nodes_core import (
    BaseNode,
    NodeData,
    UIParams,
    Array,
    NodeConfig,
    root_port,
    StructurerUIParams,
    Crypto,
)
from fabrique_nodes_core.convertors import upd_jspath_output

from pydantic import BaseModel, Field


cur_file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f"{cur_file_dir}/..")


class UIConfig(BaseModel):
    endpoint: str = Field("", title="Push Endpoint", description="Push streaming endpoint")
    api_secret_key: str = Field("", title="API secret key", description="API secret key",
                                node_updater_callback_name="make_hashed_outputs_cb")
    api_key_hash: str = Field("",
                              title="API secret key hash",
                              description="API secret key hash",
                              visible=False
                              )


class Node(BaseNode):
    """"""
    type_ = "ImplyWrite"
    category = "IO"

    initial_config = NodeData(
        name="ImplyWrite",
        g_ports_in=[
            [root_port],
        ],
        description="",
        ui_config=UIConfig(),
    )
    ui_params = UIParams(
        input_groups=[
            StructurerUIParams().input_groups[0],
        ],
        ui_config_schema=UIConfig.schema_json(),
    )

    def __init__(self, cfg: NodeConfig, user_id: str):
        super().__init__(cfg, user_id)
        self.cfg = cfg
        ui_config = UIConfig(**cfg.ui_config)
        crypto = Crypto(user_id)

        self.access_key_id = crypto.decrypt(ui_config.api_key_hash)
        self.endpoint = ui_config.endpoint

    @classmethod
    def make_hashed_outputs_cb(cls, data: NodeData, user_id: str) -> NodeData:
        if data.ui_config['api_secret_key'] == '':
            return data  # do nothing
        crypto = Crypto(user_id)
        ui_config = UIConfig(**data.ui_config)
        data.ui_config['api_key_hash'] = crypto.encrypt(ui_config.api_secret_key)
        data.ui_config['api_secret_key'] = ''
        return data

    def process(self, *inputs) -> str:
        """Create json srting

        :return: json srting
        :rtype: str
        """
        output = {}
        for i, p in enumerate([p for p in self.cfg.ports_in if p.visible]):
            if not p.code:
                output = {**output, **inputs[i]}
                continue
            upd_jspath_output(output, p.code, inputs[i])

        return [output]

    def process_universal(self, *array_args) -> list:
        nonnull_args = [a for a in array_args if a]
        if nonnull_args and isinstance(nonnull_args[0], Array):
            results_list = [self.process_universal(*args) for args in zip(*array_args) if self.is_valid_args(args)]
            return [value for values_lst in zip(*results_list) for value in values_lst]
        else:
            return self.process(*array_args)

    def process_many(self, microbatch) -> list:
        values_lst = [self.process_universal(*args) for args in zip(*microbatch) if self.is_valid_args(args)]
        payload = '\n'.join([json.dumps(val) for val_array in values_lst if val_array for val in val_array])
        headers = {
            'Authorization': f'Basic {self.access_key_id}',
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", self.endpoint, headers=headers, data=payload)
        if response.status_code != 200:
            raise Exception(response.text)
