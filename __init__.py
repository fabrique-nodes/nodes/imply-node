# flake8: noqa: F04
import sys
import os

cur_file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f"{cur_file_dir}/src")

from .src.node import Node
